<?php

declare(strict_types=1);

namespace Skadmin\InsuranceCompany;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'insurance-company';
    public const DIR_IMAGE = 'insurance-company';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-house-damage']),
            'items'   => ['overview'],
        ]);
    }
}
