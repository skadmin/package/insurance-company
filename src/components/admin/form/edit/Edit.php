<?php

declare(strict_types=1);

namespace Skadmin\InsuranceCompany\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\InsuranceCompany\BaseControl;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompany;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompanyFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private InsuranceCompanyFacade $facade;
    private LoaderFactory          $webLoader;
    private InsuranceCompany       $insuranceCompany;

    public function __construct(?int $id, InsuranceCompanyFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->insuranceCompany = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->insuranceCompany->isLoaded()) {
            return new SimpleTranslation('insurance-company.edit.title - %s', $this->insuranceCompany->getName());
        }

        return 'insurance-company.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->insuranceCompany = $this->insuranceCompany;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('code', 'form.insurance-company.edit.code');
        $form->addText('name', 'form.insurance-company.edit.name')
            ->setRequired('form.insurance-company.edit.name.req');
        $form->addCheckbox('isActive', 'form.insurance-company.edit.is-active')
            ->setDefaultValue(true);
        $form->addTextArea('content', 'form.insurance-company.edit.content');
        $form->addImageWithRFM('imagePreview', 'form.insurance-company.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.insurance-company.edit.send');
        $form->addSubmit('sendBack', 'form.insurance-company.edit.send-back');
        $form->addSubmit('back', 'form.insurance-company.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->insuranceCompany->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->insuranceCompany->getName(),
            'code'     => $this->insuranceCompany->getCode(),
            'content'  => $this->insuranceCompany->getContent(),
            'isActive' => $this->insuranceCompany->isActive(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->insuranceCompany->isLoaded()) {
            $insuranceCompany = $this->facade->update(
                $this->insuranceCompany->getId(),
                $values->name,
                $values->code,
                $values->isActive,
                $values->content,
                $identifier
            );
            $this->onFlashmessage('form.insurance-company.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $insuranceCompany = $this->facade->create(
                $values->name,
                $values->code,
                $values->isActive,
                $values->content,
                $identifier
            );
            $this->onFlashmessage('form.insurance-company.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $insuranceCompany->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }
}
