<?php

declare(strict_types=1);

namespace Skadmin\InsuranceCompany\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\InsuranceCompany\BaseControl;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompany;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompanyFacade;
use Skadmin\Translator\Translator;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private InsuranceCompanyFacade $facade;
    private ImageStorage           $imageStorage;

    public function __construct(InsuranceCompanyFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'insurance-company.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.code', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (InsuranceCompany $insuranceCompany) : ?Html {
                if ($insuranceCompany->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$insuranceCompany->getImagePreview(), '55x55', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');
        $grid->addColumnText('code', 'grid.insurance-company.overview.code');
        $grid->addColumnText('name', 'grid.insurance-company.overview.name')
            ->setRenderer(function (InsuranceCompany $insuranceCompany) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $insuranceCompany->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($insuranceCompany->getName());

                return $name;
            });
        $this->addColumnIsActive($grid, 'insurance-company.overview');

        // FILTER
        $grid->addFilterText('code', 'grid.insurance-company.overview.code');
        $grid->addFilterText('name', 'grid.insurance-company.overview.name');
        $this->addFilterIsActive($grid, 'insurance-company.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.insurance-company.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.insurance-company.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        return $grid;
    }
}
