<?php

declare(strict_types=1);

namespace Skadmin\InsuranceCompany\Doctrine\InsuranceCompany;

use App\Model\Doctrine\Traits;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;
use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class InsuranceCompany
{
    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\Code;
    use Entity\ImagePreview;

    public function update(string $name, string $code, bool $isActive, string $content, ?string $imagePreview) : void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
        $this->content  = $content;
        $this->code     = $code;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

}
