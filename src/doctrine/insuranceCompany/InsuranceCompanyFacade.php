<?php

declare(strict_types=1);

namespace Skadmin\InsuranceCompany\Doctrine\InsuranceCompany;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class InsuranceCompanyFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = InsuranceCompany::class;
    }

    public function create(string $name, string $code, bool $isActive, string $content, ?string $imagePreview) : InsuranceCompany
    {
        return $this->update(null, $name, $code, $isActive, $content, $imagePreview);
    }

    public function update(?int $id, string $name, string $code, bool $isActive, string $content, ?string $imagePreview) : InsuranceCompany
    {
        $insuranceCompany = $this->get($id);
        $insuranceCompany->update($name, $code, $isActive, $content, $imagePreview);

        $this->em->persist($insuranceCompany);
        $this->em->flush();

        return $insuranceCompany;
    }

    public function get(?int $id = null) : InsuranceCompany
    {
        if ($id === null) {
            return new InsuranceCompany();
        }

        $insuranceCompany = parent::get($id);

        if ($insuranceCompany === null) {
            return new InsuranceCompany();
        }

        return $insuranceCompany;
    }

    /**
     * @return InsuranceCompany[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, ['code' => 'ASC']);
    }
}
