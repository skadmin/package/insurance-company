<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class
Version20211226054452 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'insurance-company.overview', 'hash' => 'cf6e4c1e666edb134a90f40326f75608', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy pojišťoven', 'plural1' => '', 'plural2' => ''],
            ['original' => 'insurance-company.overview.title', 'hash' => '0cfe0bcea0ad2ce94904cf94615d77e0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy pojišťoven|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.insurance-company.overview.is-active', 'hash' => '2382de08d1b16987f6d68a091a7e5220', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.insurance-company.overview.action.new', 'hash' => '1317ac163ec98e518258141fbf658fb3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit typ pojišťovny', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.insurance-company.overview.code', 'hash' => '3ebb4a266549c167c5b4d460b7c563ce', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kód', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.insurance-company.overview.name', 'hash' => '0c9655f961ccee9fda5f88d4374da4cc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.insurance-company.overview.action.edit', 'hash' => '4c7da00fccc468323be83a0f42daa089', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'insurance-company.edit.title', 'hash' => '932739fa188b7e1eef96d4eaf6f92951', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení typu pojišťovny', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.code', 'hash' => 'ef774c1817e4758cabba610fd288217f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kód', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.name', 'hash' => 'a8f1b355590c1fd37ef3a14fd89cee48', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.name.req', 'hash' => '8ebcbfbb476b5bca6cc1ff9862e68f7a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.content', 'hash' => 'dd6de751d8522e5b779e23414a04fdb7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.image-preview', 'hash' => '00bd822b6959ba7237d2a70611930a74', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Logo', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.image-preview.rule-image', 'hash' => 'e4fdcbfd9c46151a23fdc2c93884b62d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.is-active', 'hash' => '3c7af51e0b2fafb85ab5025514bb92e4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.send', 'hash' => '858c71704fab9dbdb880a0be253e22aa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.send-back', 'hash' => '0eb0d17d76e6d5ff07437994086ef1fd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.back', 'hash' => 'b97287ed1c7bba4aefc32f01b172faa5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.flash.success.create', 'hash' => 'a89dd5513025191db309e58681049b38', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ pojišťovny byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'insurance-company.edit.title - %s', 'hash' => '1e1e0dc179cc2e7eb9366fb7ee09b44b', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace typu pojišťovny', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.insurance-company.edit.flash.success.update', 'hash' => 'f871cbf52e860d7d9cab32bfdcc80af8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ pojišťovny byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
